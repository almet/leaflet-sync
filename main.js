import L from "leaflet";
import "leaflet/dist/leaflet.css";

// Initialize the map and set its view to our chosen geographical coordinates and a zoom level:
const map = L.map("map").setView([48.1173, -1.6778], 13);

// Add a tile layer to add to our map, in this case using Open Street Map
L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  maxZoom: 19,
  attribution: "© OpenStreetMap contributors",
}).addTo(map);

// Add new features to the map with a click
function onMapClick(e) {
  new L.marker(e.latlng, {
    draggable: true,
    autoPan: true,
  }).addTo(map);
}

map.on("click", onMapClick);
