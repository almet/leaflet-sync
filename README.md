# Experiments on real-time map collaboration

This repository contains experiments on different ways to add sync to a leaflet map, using Conflict-free Resolution DataTypes (CRDTs).
This main branch isn't really interesting.

The code for each library I tried is on a specific branch, and does (almost) the same thing: expect a map with different markers on it, and when you create or move one marker, it syncs with the other connected clients.

- [yjs (code)](https://gitlab.com/almet/leaflet-sync/-/tree/yjs), using [yjs](https://github.com/yjs/yjs).
- [json-joy (code)](https://gitlab.com/almet/leaflet-sync/-/tree/jsonjoy) using [json-joy](https://jsonjoy.com).
- [automerge (code)](https://gitlab.com/almet/leaflet-sync/-/tree/automerge), using [automerge](https://automerge.org).

Explorations not finished:

- [vulcan (code)](https://gitlab.com/almet/leaflet-sync/-/tree/vulcan), a local-first approach using (and syncing) SQLite databases using [vulcan](https://vlcn.io).
